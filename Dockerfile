FROM openjdk:17
LABEL authors="Tidiany"

COPY target/tp-jakarta-ee-devops.jar tp-jakarta-ee-devops.jar
#VOLUME /tmp
EXPOSE 8089
ENTRYPOINT ["java", "-jar", "tp-jakarta-ee-devops.jar"]

#FROM maven:3.8.5-openjdk-17
#LABEL authors="Tidiany"
#WORKDIR /tp-spring-boot-sonar
#COPY . /tp-spring-boot-sonar
#RUN mvn clean install -DskipTests
#CMD mvn spring-boot:run
